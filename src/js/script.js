import EmblaCarousel from 'embla-carousel'
import { setupDotBtns, generateDotBtns, selectDotBtn } from "./dotButtons";
// Grab wrapper nodes
const rootNode = document.querySelector('.embla')
const viewportNode = rootNode.querySelector('.embla__viewport')

// Grab button nodes
// Grab button nodes
const prevButtonNode = rootNode.querySelector('.embla__prev')
const nextButtonNode = rootNode.querySelector('.embla__next')

// Initialize the carousel
const embla = EmblaCarousel(viewportNode)

// Add click listeners
prevButtonNode.addEventListener('click', embla.scrollPrev, false)
nextButtonNode.addEventListener('click', embla.scrollNext, false)